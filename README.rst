
What is Tox?
--------------------

Tox is a generic virtualenv management and test command line tool you can use for:

* checking your package installs correctly with different Python versions and
  interpreters

* running your tests in each of the environments, configuring your test tool of choice

* acting as a frontend to Continuous Integration servers, greatly
  reducing boilerplate and merging CI and shell-based testing.

For more information and the repository please checkout:

- homepage: http://tox.testrun.org

- repository: https://bitbucket.org/hpk42/tox


have fun,

holger krekel, 2015


What is Guix-tox ?
------------------
Guix-tox is a modified version of tox that can use "guix[1] environment"
instead of virtualenv.

Issues with virtualenv:

- wrong abstraction level : tox uses virtual environments instead of a robust
  package manager.
- packages not found on Pypi are not handled : for instance, with virtualenv,
  tox can install python-mysql, but not MySQL; Guix can install both.
- waste of disk space : if two virtual environments need the same dependency,
  it will be copied twice on the hard drive; with Guix, it is only stored once
  in /gnu/store/.
- lack of reproducibility : using virtualenv, there is no isolation from the
  rest of the system; Guix provides that.


Current issues of guix-tox:

- lack of packages : tox can download any Python package on Pypi, while Guix
  can only build those packaged for Guix.
- building and installing extensions from source is not supported.
- lack of testing.
- only Python 2.7 and 3.4 can be used.

Using guix-tox:

You will need to have Guix installed and running.

Run "python setup.py install", as you would with an unmodified version of tox. You should then have a "guix-tox" binary in your $PATH.

You can then run guix-tox almost as you would run tox:

$ cd trollius/

$ guix-tox --env=guix -epy34

...

py34 Guix env: guix environment --ad-hoc --pure python-aiotest python-setuptools python-pbr python --exec=PYTHON=python3.4 python3.4 runtests.py -r

...

  py34: commands succeeded

Note that you could use "--env=virtualenv" to use the default implementation of
tox. This is the default setting.



[1] http://www.gnu.org/software/guix/
